///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animalFactory.hpp
/// @version 1.0
///
/// Returns a random Animal
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   Mar 13 2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

using namespace std;

namespace animalfarm {

class AnimalFactory {
public: 
    static Animal* getRandomAnimal(); 

};


} //namespace animalfarm





