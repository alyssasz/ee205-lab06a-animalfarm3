///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Generates an Array and a List of random Animals speaking
///
/// @author Alyssa Zhang <alyssasz@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   Mar 13 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animalFactory.hpp"
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {

    cout << "Welcome to Animal Farm 3" << endl;

    std::array<Animal*, 30> animalArray;
    animalArray.fill(NULL);
    for (int i = 0; i < 25; i++) {
        animalArray[i] = AnimalFactory::getRandomAnimal();
    }
    cout << endl;

    cout << std::boolalpha;
    cout << "Array of Animals:"       << endl;
    cout << "   Is it empty: "        << animalArray.empty()    << endl;
    cout << "   Number of elements: " << animalArray.size()     << endl;
    cout << "   Max size: "           << animalArray.max_size() << endl;

    //iterating over the array 
    for ( Animal* animal : animalArray) {      
        if (animal == NULL) {
            break;
        }

        cout << animal->speak() << endl;
    }

    // fires up the destructor for animalArray
    for ( Animal* animal : animalArray) {
        if (animal == NULL) {
            break;
        }

        delete animal;
    }

    std::list<Animal*> animalList;
    for (int i = 0; i < 25; i++) {
        animalList.push_front(AnimalFactory::getRandomAnimal());
    }
    cout << endl;

    cout << "List of Animals:"        << endl;
    cout << "   Is it empty: "        << animalList.empty()    << endl;
    cout << "   Number of elements: " << animalList.size()     << endl;
    cout << "   Max size: "           << animalList.max_size() << endl;

    //iterating over the list
    for ( Animal* animal : animalList) {      
        if (animal == NULL) {
            break;
        }

        cout << animal->speak() << endl;
    }

    // fires up the destructor for animalList
    for ( Animal* animal : animalList) {
        if (animal == NULL) {
            break;
        }

        delete animal;
    }
    cout << endl;
    
    // clears out the animalArray and animalList
    /* animalArray.fill(0);
    cout << animalArray.empty() << endl;

    animalList.clear();
    cout << animalList.empty() << endl;
    */

    return 0;

}
